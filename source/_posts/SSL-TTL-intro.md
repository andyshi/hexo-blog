---
title: SSL/TLS协议入门概述
layout: post
date: 2019-06-23 22:51:12
categories: technique
tags:
---


> 互联网的通信安全，建立在SSL/TLS协议之上。本文简要介绍SSL/TLS协议的运行机制。
> 文章的重点是设计思想和运行过程，不涉及具体的实现细节。如果想了解这方面的内容，请参阅RFC文档。